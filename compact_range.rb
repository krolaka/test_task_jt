# frozen_string_literal: true

# Part 1 test task 1
class CompactRange
  attr_reader :text, :ranges, :range

  def initialize(text)
    @text = text
    @ranges = []
    @range = ''
  end

  def compact
    (text.size - 1).times do |itheration|
      select_method(itheration)
    end
    replace_phrase
  end

  private

  def select_method(itheration)
    if neighbours(text[itheration], text[itheration + 1])
      @range += text[itheration]
    else
      @range += text[itheration]
      ranges << range
      @range = ''
    end
  end

  def replace_phrase
    result = text.dup
    @ranges = @ranges.reject { |r| r.length <= 2 }
    @ranges.each { |range| result.sub!(range.slice(1..-2), '-') }
    result
  end

  def neighbours(sym1, sym2)
    (sym1.ord - sym2.ord).abs == 1
  end
end

puts CompactRange.new('abcdab987612').compact
