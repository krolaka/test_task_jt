# frozen_string_literal: true

# deep_merge and deep_merge! monkey patch
class Hash
  def deep_merge(other_hash, &block)
    dup.deep_merge!(other_hash, &block)
  end

  def deep_merge!(other_hash, &block)
    merge!(other_hash) do |_, this_val, other_val|
      if this_val.is_a?(Hash) && other_val.is_a?(Hash)
        this_val.deep_merge(other_val, &block)
      elsif block_given?
        yeild
      else
        other_val
      end
    end
  end
end

# Part 2 test task 1
class TranslateYaml
  require 'yaml'

  attr_reader :file_name, :new_file_name, :result_hash

  def initialize
    @file_name = 'translations_simple.yml'
    @new_file_name = 'translations.yml'
    @result_hash = {}
  end

  def translate
    return puts 'File not found' unless File.file?(file_name)

    translate_to_hash(parse_yml)
    save_translation
  end

  private

  def translate_to_hash(yml_data)
    yml_data.each do |key, item|
      @result_hash = create_hash(key, item)
    end
  end

  def create_hash(key, item)
    line = {}
    key.split('.').reverse.each_with_index do |el, idx|
      line = idx.zero? ? { el => item } : { el => line }
    end
    @result_hash.deep_merge(line)
  end

  def parse_yml
    YAML.safe_load(File.open(file_name))
  end

  def save_translation
    File.open(new_file_name, 'w') { |file| file.write(@result_hash.to_yaml) }
  end
end

TranslateYaml.new.translate
