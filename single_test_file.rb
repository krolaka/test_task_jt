# frozen_string_literal: true

require 'bundler/inline'

gemfile(true) do
  source 'https://rubygems.org'

  gem 'rails'
  gem 'sqlite3'
  gem 'factory_bot_rails'
  gem 'minitest-rails'
end

require 'active_record'
require 'action_controller/railtie'
require 'minitest/autorun'

Rails.env = 'test'
ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'test_db')
ActiveRecord::Base.logger = Logger.new(STDOUT)

# Migrations
ActiveRecord::Schema.define do
  create_table :users, force: true do |t|
    t.string :name
    t.timestamps
  end

  create_table :projects, force: true do |t|
    t.string :name
    t.timestamps
  end

  create_table :roles, force: true do |t|
    t.references :user
    t.references :project
    t.integer :role, default: 0
    t.timestamps
  end
end

# User model
class User < ActiveRecord::Base
  has_many :roles
  has_many :projects, through: :roles
end

# Project model
class Project < ActiveRecord::Base
  has_many :roles
  has_many :users, through: :roles
end

# Role model
class Role < ActiveRecord::Base
  enum role: %i[user admin]

  belongs_to :project
  belongs_to :user
end

# Factory initialization
FactoryBot.define do
  factory :role

  factory :user do
    sequence(:name) { |n| %w[Paul Neeraj][1 ^ n % 2] }
  end

  factory :project do
    sequence(:name) { |n| %w[JetThought BigBinary][1 ^ n % 2] }
  end
end

# Tests
class SingleTestFile < Minitest::Test
  def self.create_roles(users, projects)
    users.each_with_index do |user, idx|
      select_role = idx.zero? ? Role.roles[:admin] : Role.roles[:user]
      2.times do |i|
        data = { project_id: projects[i].id, role: select_role ^ i }
        user.roles.create(data)
      end
    end
  end

  def self.prepare_data
    users = FactoryBot.create_pair(:user)
    projects = FactoryBot.create_pair(:project)
    create_roles(users, projects)
  end
  prepare_data

  def test_non_admin_users_in_jt_project
    project = Project.find_by_name('JetThought')
    users = project.roles
                   .joins(:user)
                   .where(role: Role.roles[:user])
                   .map { |res| res.user.name }
    assert_equal %w[Neeraj], users
  end

  def test_find_projects_paul_is_admin
    user = User.find_by_name('Paul')
    names = user.roles
                .joins(:project)
                .where(role: Role.roles[:admin])
                .map { |res| res.project.name }
    assert_equal %w[JetThought], names
  end

  def test_find_projects_paul_is_admin_optimized
    projects = Project.joins(:roles, :users)
                      .where(
                        roles: { role: Role.roles[:admin] },
                        users: { name: 'Paul' },
                        roles_projects_join: { role: Role.roles[:admin] }
                      ).pluck(:name)
    assert_equal %w[JetThought], projects
  end
end
