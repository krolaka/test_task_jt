# frozen_string_literal: true

# Part 1 test task 2

string = 'To be or not to be that is the question'

def wrap_text(text, width)
  lines = text.split(/(.{1,#{width}}(\s+))|(.{1,#{width}})/) - ['', ' ']
  lines.join("\n")
end

puts wrap_text(string, 5)
